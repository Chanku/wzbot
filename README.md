#WZ-IRC Bot

##What is WZ-IRC Bot?  
It's a basic, but extensible, IRC Bot program. 

##Requirements  
Python 3 (3.4 and higher reccomended)

##Installation  
1. Do [insert code stuff here]
2. Do python3 start.py \[server\_hostname\] \[server\_port\]

##PFAQ
###Why Python 3?  
It's the only thing I know?

###Why not use OOP?  
It's a horribly inefficent paradigm that leads to the creation of rigid and brittle programs. 

###Why an IRC Bot?  
IRC is easy. 

###Why do you not use Unit Testing?  
I really don't understand it on some level. That's not to say I can't do it, I just don't understand the concept and it's purpose. Further I avoid OOP where possible. 
