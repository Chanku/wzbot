import sys
import os
import importlib

def reload_plugins(command_dict):
    for module in command_dict:
        new_module = importlib.reload(module)
        del command_dict[module]
        command_dict[new_module] = []
        try:
            command_dict[new_module].append(new_module.register_command_known())
            command_dict[new_module].append(new_module.register_command_unknown())
        except AttributeError:
            try:
                command_dict[new_module].append(new_module.register_command_known())
            except AttributeError:
                command_dict[new_module].append(new_module.register_command_unknown())
    return command_dict

def load():
    plugin_list = _import_modules()
    command_dict = {}
    for plugin in plugin_list:
        command_dict[plugin] = []
        try:
            command_dict[plugin].append(plugin.register_command_known())
            command_dict[plugin].append(plugin.register_command_unknown())
        except AttributeError as e:
            print(str(e))
            try:
                command_dict[plugin].append(plugin.register_command_known())
            except AttributeError as e:
                print(str(e))
                command_dict[plugin].append(plugin.register_command_unknown())
    print(command_dict)
    return command_dict

def unload(plugin_name):
    del command_dict[plugin_name]

def _import_modules():
    module_list = []
    for item in os.listdir('./plugins'):
        if item.endswith('.py') and "__init__" not in item:
            module_list.append(importlib.import_module('plugins.{}'.format(item[:len(item) - 3])))
    return module_list

def _get_message(raw_message):
    sMsg = raw_message
    stMsg = raw_message.strip('\r\n')
    try:
        message_stuff = {'sender':sMsg.split(' PRIVMSG ')[0].split('!')[0][1:],
                         'channel':sMsg.split(' PRIVMSG ')[1].split(' :')[0].strip(' ')}
        if len(stMsg.split(message_stuff['channel'])) > 2:
            i = 1
            x = stMsg.split(message_stuff['channel'])
            while i < len(x):
                try:
                    message_stuff['message'] = "{}{}{}".format(message_stuff['message'],
                                                               message_stuff['channel'],
                                                               x[i])
                    i += 1
                except KeyError:
                    message_stuff['message'] = x[i][2:]
                    i += 1
        else:
            message_stuff['message'] = stMsg.split(message_stuff['channel'])[1][2:]
        message_stuff['mtype'] = 'message'
    except IndexError:
        try:
            message_stuff = {'sender':sMsg.split(' JOIN ')[0].split('!')[0][1:],
                             'channel':sMsg.split(' JOIN ')[1].strip(' ').strip('\n').strip('\r'),
                             'message':stMsg,
                             'mtype':'join'}
        except IndexError:
            try:
                message_stuff = {'sender':sMsg.split(' PART ')[0].split('!')[0][1:],
                                 'channel':sMsg.split(' PART ')[1].split(' :')[0].strip(' '),
                                 'message':stMsg,
                                 'mtype':'part'}
            except IndexError:
                try:
                    message_stuff = {'sender':sMsg.split(' QUIT ')[0].split('!')[0][1:],
                                    'channel':sMsg.split(' QUIT ')[1].split(' :')[0].strip(' '),
                                    'message':stMsg,
                                    'mtype':'quit'}
                except IndexError:
                    try:
                        message_stuff = {'sender':sMsg.split(' TOPIC ')[0].split('!')[0][1:],
                                         'channel':sMsg.split(' TOPIC ')[1].split(' :')[0].strip(' '),
                                         'message':sMsg.split(' TOPIC ')[1].split(' :')[1],
                                         'mtype':'topic-change'}
                    except IndexError:
                        try:
                            message_stuff = {'sender':sMsg.split(' NICK ')[0].split('!')[0][1:],
                                             'channel':'',
                                             'message':sMsg.split(' NICK ')[1].split(" :")[0],
                                             'mtype':'nick-change'}
                        except IndexError:
                            try:
                                message_stuff = {'sender':'',
                                                 'channel':sMsg.split(' 333 ')[1].split(' :')[0].strip(' '),
                                                 'message':stMsg,
                                                 'mtype':'topic'}
                            except IndexError:
                                try:
                                    message_stuff = {'sender':'',
                                                     'channel':sMsg.split(' 353 ')[1].split(' = ')[1].split(' :')[0].strip(' '),
                                                     'message':sMsg.split(' 353 ')[1].split(' = ')[1].split(' :')[1].strip(' '),
                                                     'mtype':'user-list'}
                                except IndexError:
                                    try:
                                        message_stuff = {'sender':'',
                                                         'channel':sMsg.split(' MODE ')[1].split(' +')[0].split(' -')[0].strip(' '),
                                                         'message':stMsg,
                                                         'mtype':'mode'}
                                    except:
                                        try:
                                            message_stuff = {'sender':'',
                                                             'channel':'',
                                                             'message':stMsg.split('PING :')[1],
                                                             'mtype':'ping'}
                                        except IndexError:
                                            return False
    return message_stuff
