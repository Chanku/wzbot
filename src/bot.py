#!/usr/bin/python3
import socket
import sys
import time
import multiprocessing
import plugin_matrix

def start_connection(hostname, port, bot_nick="WZBOT", bot_user="WZBOT", encoding='utf-8'):
    irc_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    irc_socket.connect((hostname, port))
    irc_socket.send(("PASS SAPEINT\r\n").encode(encoding))
    irc_socket.send(("NICK " + bot_nick + '\r\n').encode(encoding))
    irc_socket.send(("USER " + bot_user + " 0  *  :Sapein's Warzone IRC Bot\r\n").encode(encoding))
    while True:
        rMsg = irc_socket.recv(2048)
        sMsg = rMsg.decode(encoding)
        print(sMsg)
        if "ping" in str(sMsg).lower():
            sReply = sMsg.split(":")[1]
            irc_socket.send(("PONG :" + sReply + '\r\n').encode(encoding))
        if 'welcome' in str(sMsg).lower():
            break
    irc_socket.recv(2048)
    return irc_socket

def main(irc_socket = None, command_dict = None, channel_list =  None, arguments = None, rload = False):
    try:
        if len(sys.argv) <= 1 and not rload:
            raise IndexError
        elif len(sys.argv) > 1 or rload:
            channels = {}
            if not rload:
                arguments = {}
                channels = {}
                for item in sys.argv:
                    if sys.argv.index(item) != 0:
                            if item.startswith('nck:') or item.startswith('usr:') or item.startswith('enc:') or (
                                item.startswith('chn:')):
                                if item.startswith('nck:'):
                                    arguments['bot_nick'] = item[4:]
                                elif item.startswith('usr:'):
                                    arguments['bot_user'] = item[4:]
                                elif item.startswith('enc:'):
                                    arguments['encoding'] = item[4:]
                                elif item.startswith('chn:'):
                                    split_item = item[4:].split(',')
                                    for item in split_item:
                                        channels[item] = 'not connected'
                            else:
                                if len(arguments) == 0:
                                    arguments['hostname'] = str(item)
                                elif len(arguments) == 1:
                                    arguments['port'] = int(item)
            if not irc_socket and not rload:
                irc_socket = start_connection(**arguments)
            if not channel_list and not rload:
                for channel in channels:
                    try:
                        irc_socket.send((('JOIN {}\r\n').format(channel)).encode(arguments['encoding']))
                        channels[channel] = 'connected'
                    except KeyError:
                        arguments['encoding'] = 'utf-8'
                        irc_socket.send((('JOIN {}\r\n').format(channel)).encode('utf-8'))
                        channels[channel] = 'connected'
            if rload:
                for channel in channel_list:
                    channels[channel] = 'connected'
                del channel_list
            if not command_dict:
                command_dict = plugin_matrix.load()
            if rload:
                print(channels)
                for channel in channels:
                    irc_socket.send("PRIVMSG {} :RESTART COMPLETE!\r\n".format(channel).encode(arguments['encoding']))
            while True:
                rMsg = irc_socket.recv(2048)
                try:
                    sMsg = rMsg.decode(arguments['encoding'])
                except KeyError:
                    arguments['encoding'] = 'utf-8'
                    sMsg = rMsg.decode(arguments['encoding'])
                stMsg = sMsg.strip('\r\n')
                for plugin in command_dict:
                    for command_type in command_dict[plugin]:
                        if isinstance(command_type, dict):
                            for command in command_type:
                                if command in sMsg:
                                    data = command_type[command]()
                                    if (data['response_type'] == 'msg' or
                                        data['response_type'] == 'message'):
                                        channel = sMsg.split(' PRIVMSG ')[1].split(' :')[0].strip(' ')
                                        irc_socket.send((('PRIVMSG {} :{}\r\n').format(channel,
                                                        data['returned_output'])).encode(arguments['encoding']))
                        elif isinstance(command_type, list):
                            for commands in command_type:
                                message_stuff = plugin_matrix._get_message(sMsg)
                                if not message_stuff:
                                    break
                                data = commands(**message_stuff)
                                if (data['response_type'] == 'msg' or
                                    data['response_type'] == 'message'):
                                    channel = message_stuff['channel']
                                    irc_socket.send((('PRIVMSG {} :{}\r\n').format(channel,
                                                    data['returned_output'])).encode(arguments['encoding']))
                                    if '.reload_all' in sMsg:
                                        command_dict = plugin_matrix.load()
                                        irc_socket.send((("PRIVMSG {} :RELOADING COMPLETE!\r\n"
                                                         ).format(channel)
                                                        ).encode(arguments['encoding']))
                                        break
                                    elif '.reload' in sMsg:
                                        command_dict = plugin_matrix.reload_plugins(command_dict)
                                        irc_socket.send((("PRIVMSG {} :RELOADING COMPLETE!\r\n"
                                                         ).format(channel)
                                                        ).encode(arguments['encoding']))
                                        break
                                elif data['response_type'] == 'chanserv':
                                    irc_socket.send((("PRIVMSG chanserv :{}\r\n"
                                                     ).format(data['returned_output'])
                                                    ).enocde(arguments['encoding']))
                                elif data['response_type'] == 'nickserv':
                                    irc_socket.send((("PRIVMSG nickserv :{}\r\n"
                                                     ).format(data['returned_output'])
                                                    ).enocde(arguments['encoding']))
                                elif data['response_type'] == 'action':
                                    channel = message_stuff['channel']
                                    irc_socket.send((('PRIVMSG {} :\x01ACTION {}\x01\r\n').format(channel,
                                                    data['returned_output'])).encode(arguments['encoding']))
                                elif data['response_type'] == 'raw':
                                    irc_socket.send(('{}\r\n'.format(data['returned_output']).encode(arguments['encoding'])))
                                    if '.quit' in sMsg:
                                        sys.exit()
                                    elif '.join' in sMsg:
                                        channels[message_stuff['channel']] = 'connected'
                                        break
                                    elif '.restart' in sMsg:
                                        for channel in channels:
                                            irc_socket.send(('{}\r\n'.format(data['returned_output'].format(channel))
                                                            ).encode(arguments['encoding']))
                                        new_bot = multiprocessing.Process(target=main, args=(irc_socket,command_dict,channels,arguments,
                                                                                             True,))
                                        new_bot.start()
                                        del irc_socket
                                        del command_dict
                                        new_bot.join()
                                        sys.exit()
                                    elif '.restartc' in sMsg:
                                        for channel in channels:
                                            irc_socket.send(('{}\r\n'.format(data['returned_output'].format(channel))
                                                            ).encode(arguments['encoding']))
                                        new_bot = multiprocessing.Process(target=main, args=(irc_socket, None, channels, arguments, True,))
                                        new_bot.start()
                                        del irc_socket
                                        del command_dict
                                        new_bot.join()
                                        sys.exit()
    # except IndexError as e:
    #     print(str(e))
    #     print("ERROR: Not enough argument(s) passed!")
    #     print("You need to give the hostname and port arguments")
    #     sys.exit(1)
    except socket.gaierror:
        print("ERROR: Hostname/Address is invalid!")
        print("Try changing the address/hostname or check your connection!")
        sys.exit(1)
    except ConnectionRefusedError:
        print("ERROR: Connection Refused!")
        print("Try changing the port used, or try 6667!")
        sys.exit(1)
    except ConnectionResetError as e:
        print("ERROR: Connection reset by peer!")
        print("Report this error to the developer.")
        print(str(e))
        sys.exit(1)

if __name__ == "__main__":
    main()
