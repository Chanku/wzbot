Owner = "Chanku"
def register_command_unknown():
    return [_quit, _join, _leave, _reload_plugins, _reload_everything, _restart, _restart_complete, _pong]

def _pong(message, sender, channel, mtype):
    if mtype == 'ping':
        return {'response_type':'raw', 'returned_output':'PONG :{}'.format(message)}
    else:
        return {'response_type':''}

def _quit(message, sender, channel, **kwargs):
    if sender.lower() == Owner.lower():
        if message.lower().startswith('.quit'):
            try:
                return {'response_type':'raw', 'returned_output':'QUIT :{}'.format(message.split('.quit ')[1])}
            except IndexError:
                return {'response_type':'raw', 'returned_output':'QUIT'}
    return {'response_type':''}

def _join(message, sender, channel, **kwargs):
    if sender.lower() == Owner.lower():
        if message.lower().startswith('.join'):
            return {'response_type':'raw', 'returned_output':'JOIN {}'.format(message.split('.join ')[1])}
    return {'response_type':''}

def _leave(message, sender, channel, **kwargs):
    if sender.lower() == Owner.lower():
        if message.lower().startswith('.leave') or message.lower().startswith('.part'):
            try:
                return {'response_type':'raw', 'returned_output':'PART {}'.format(message.split('.leave ')[1])}
            except IndexError:
                try:
                    return {'response_type':'raw', 'returned_output':'PART {}'.format(message.split('.part ')[1])}
                except IndexError:
                    return {'response_type':'raw', 'returned_output':'PART {}'.format(channel)}
    return {'response_type':''}

def _reload_plugins(message, sender, channel, **kwargs):
    if sender.lower() == Owner.lower():
        if message.lower().startswith('.reload') and not message.lower().startswith('.reload_all'):
            return {'response_type':'msg', 'returned_output':'STARTING PLUGIN RELOAD - PLEASE WAIT...'}
    return {'response_type':''}

def _reload_everything(message, sender, channel, **kwargs):
    if sender.lower() == Owner.lower():
        if message.lower().startswith('.reload_all'):
            return {'response_type':'msg', 'returned_output':'RELOADING EVERY PLUGIN - PLEASE WAIT...'}
    return {'response_type':''}

def _restart(message, sender, channel, **kwargs):
    if sender.lower() == Owner.lower():
        if message.lower().startswith('.restart'):
            print('restarting')
            return {'response_type':'raw', 'returned_output':'PRIVMSG {} :RESTARTING THE BOT. PLEASE WAIT!'}
    return {'response_type':''}

def _restart_complete(message, sender, channel, **kwargs):
    if sender.lower() == Owner.lower():
        if message.lower().startswith('.restartc'):
            return {'response_type':'raw', 'returned_output':'PRIVMSG {} :RESTARTING THE BOT. PLEASE WAIT!'}
    return {'response_type':''}
