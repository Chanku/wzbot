def register_command_known():
    return {'.test2':_test_command_2}

def register_command_unknown():
    return [_test_command]

def _test_command(message, sender, channel, **kwargs):
    if message == ".test":
        return {'response_type':'msg', 'returned_output':'TEST!'}
    else:
        return {'response_type':''}

def _test_command_2():
    return {'response_type':'msg', 'returned_output':'testerooni!'}
